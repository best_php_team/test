<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "answers".
 *
 * @property int $id
 * @property int $label
 * @property string $answer
 * @property int $isright
 * @property int $question_id
 * @property int $created_at
 * @property int $updated_at
 */
class Answers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['label', 'answer', 'isright', 'question_id'], 'required'],
            [[ 'question_id'], 'integer'],
            [['answer','label'], 'string'],
            [['isright', ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'label' => Yii::t('app', 'Label'),
            'answer' => Yii::t('app', 'Answer'),
            'isright' => Yii::t('app', 'Isright'),
            'question_id' => Yii::t('app', 'Question ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::className()],
        ];
    }

}
