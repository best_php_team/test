<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "user".
 *
 * @property string $login
 * @property string $password
 */
class SignUp extends Model
{
    public $password;
    public $login;

    public function rules()
    {
        return [
            [['login','password'], 'required'],
            [['login','password'], 'string'],
            [['login'], 'unique','targetClass' => User::className(),'message' => 'Login number already exists'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'login' => Yii::t('app', 'Login'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        $user = new User();
        $user->login = $this->login;
        $user->generateAuthKey();
        $user->generateToken();
        $user->status = User::USER_ACTIVE;
        $user->setPassword($this->password);
        $user->created_at = time();
        $user->updated_at = time();
        if ($user->validate() && $user->save()) {
            Yii::$app->session->setFlash('success','New account successfully created!');
            return true;
        } else {
            Yii::$app->session->setFlash('error','Something went wrong!');
            return false;
        }
    }




}
