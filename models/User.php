<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property int $user_group
 * @property int $role
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property int $status
 * @property string $lastname
 * @property string $firstname
 * @property string $middlename
 * @property int $created_at
 * @property int $updated_at
 */

class User extends ActiveRecord implements IdentityInterface
{
    const USER_ACTIVE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_group', 'role', 'status', 'created_at', 'updated_at'], 'integer'],
            [['password_hash', 'password_reset_token', 'auth_key'], 'string'],
            [['login', 'lastname', 'firstname', 'middlename'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 70],
            [['login'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'login' => Yii::t('app', 'Login'),
            'email' => Yii::t('app', 'Email'),
            'user_group' => Yii::t('app', 'User Group'),
            'role' => Yii::t('app', 'Role'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'status' => Yii::t('app', 'Status'),
            'lastname' => Yii::t('app', 'Lastname'),
            'firstname' => Yii::t('app', 'Firstname'),
            'middlename' => Yii::t('app', 'Middlename'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public static function dropDownList()
    {
        return ArrayHelper::map(self::findAll(['status' => User::USER_ACTIVE]), 'id', 'fullname');
    }

    /**
     * @param int|string $id
     * @return User|null|IdentityInterface
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return void|IdentityInterface
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User|null|string
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return User|bool|null
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key == $authKey;
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function generateToken()
    {
        return $this->accessToken = Yii::$app->security->generateRandomString();
    }

    /**
     * @param $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public static function hasRole($roleName, $userId)
    {
        $authManager = \Yii::$app->getAuthManager();
        return $authManager->getAssignment($roleName, $userId) ? true : false;
    }

    public static function usersByRole($roleName)
    {
        $authManager = \Yii::$app->getAuthManager();
        return $authManager->getUserIdsByRole($roleName);
    }

    public static function canUpdate($owner)
    {
        return ($owner === Yii::$app->user->identity->id || User::hasRole('ADMINISTRATOR', Yii::$app->user->identity->id)) ? true : false;
    }

    public function getFio(){
        $ret = $this->lastname." ".substr($this->firstname, 0, 1).".".substr($this->middlename, 0, 1).".";
        if(strlen($ret)<6){
            return $this->login;
        }else{
            return $ret;
        }
    }

}
