<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "group_subject".
 *
 * @property int $g_id
 * @property int $s_id
 *
 * @property Subject $subject
 */
class GroupSubject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group_subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['g_id', 's_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'g_id' => Yii::t('app', 'G ID'),
            's_id' => Yii::t('app', 'S ID'),
        ];
    }
    public function getSubject(){
        return $this->hasOne(Subject::className(), ['id'=>'s_id']);
    }
}
