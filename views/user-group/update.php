<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\UserGroup */

$this->title = Yii::t('app', 'Update User Group: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = Yii::t('app', 'User Groups');
$this->params['breadcrumbs'][] = $model->title;
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');


$this->registerCssFile('/web/css/bower_components/select2/dist/css/select2.min.css');
$this->registerJsFile('/web/css/bower_components/select2/dist/js/select2.full.min.js', ['depends'=>'app\asset\AppAsset']);

?>
<div class="user-group-update">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <?php $form = ActiveForm::begin(); ?>
                <div class="box-body">
                    <?= $form->field($model, 'title',['template'=>'<div class="form-group">{label}{input}<div class="col-md-12">{error}</div></div>'])->textInput(['maxlength' => true]) ?>
                </div>
                <div class="box-body">
                    <?$categoryList = \app\models\Category::dropList();?>
                    <?= $form->field($model, 'category_id',['template'=>'<div class="form-group">{label}{input}<div class="col-md-12">{error}</div></div>'])->dropDownList($categoryList,['class'=>'form-control select2 auto-complete-select', 'data-child'=>'usergroup-subject_id', 'data-url'=>Url::to('/user-group/subject-by-category')]) ?>
                </div>
                <div class="box-body">
                    <?= $form->field($model, 'subject_id',['template'=>'<div class="form-group">{label}{input}<div class="col-md-12">{error}</div></div>'])->dropDownList(\app\models\Subject::dropListByCategory(array_key_first($categoryList)),['class'=>'form-control select2', 'multiple'=>true]) ?>
                </div>
                <div class="box-footer">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>
