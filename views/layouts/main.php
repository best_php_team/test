<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\asset\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="sidebar-mini skin-green-light <?=isset($_COOKIE['sidebar-collapse']) && $_COOKIE['sidebar-collapse'] == 'mini'?"sidebar-collapse":""?>">
<?php $this->beginBody();
$c = Yii::$app->controller->id;
$a = Yii::$app->controller->action->id;
$u = Yii::$app->user->identity;
?>
<style>
    @media print {
        footer{
            display: block !important;
            position: absolute !important;
            bottom: 0 !important;
            right: 0 !important;
            border: none !important;
        }
    }
</style>
    <?=Yii::$app->controller->renderPartial('//blocks/header',['user'=>$u])?>
    <?=Yii::$app->controller->renderPartial('//blocks/left',[
        'action'=>$a,
        'controller'=>$c,
        'user'=>$u
    ])?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?=  Yii::$app->controller->getView()->params['breadcrumbs'][0];  ?>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> <?=Yii::t('app','Home')?></a></li>
                <li class="active"><?=Yii::$app->controller->getView()->params['breadcrumbs'][0]?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?= $content ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

<?=Yii::$app->controller->renderPartial('//blocks/footer')?>

<?php $this->endBody() ?>
<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree()
        $('.select2').select2()
    });
    $(".auto-complete-select").change(function(){
        console.log($(this).val());
        var select = $(document).find("#"+$(this).data("child")).empty();

        $.get($(this).data("url")+"/"+$(this).val(),function (data) {
            data=$.parseJSON(data);
            for(var i=0; i < data.length; i++){
                var option = new Option(data[i].title, data[i].id, false, false);
                select.append(option);
                select.trigger({
                    type: 'select2:select',
                    params: {
                        data: data[i]
                    }
                });
            }
        });

    });
    var sbclp = "<?=isset($_COOKIE['sidebar-collapse']) && $_COOKIE['sidebar-collapse'] == 'mini'?"maxi":"mini"?>" ;

    $('.sidebar-toggle').on('click', function () {
        document.cookie = "sidebar-collapse="+sbclp+"; path=/"
        if(sbclp === "mini"){
            sbclp = "maxi"
        }else{
            sbclp = "mini"
        }
    })
</script>
</body>
</html>
<?php $this->endPage() ?>
