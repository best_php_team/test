<?php
/**
 * Created by PhpStorm.
 * User: Sanjar
 * Date: 19.01.2020
 * Time: 19:45
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Journal');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-tools">
                        <?$form = \yii\widgets\ActiveForm::begin(['action'=>'/journal/subjects/'.$id,'method'=>'get'])?>
                        <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                            <input type="text" name="title" class="form-control pull-right" value="<?=$searchModel->title?>" placeholder="<?=Yii::t('app','Search')?>">
                            <div class="input-group-btn"><button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button></div>
                        </div>
                        <? \yii\widgets\ActiveForm::end()?>
                    </div>

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>№</th>
                            <th><?=$searchModel->getAttributeLabel('title')?></th>
                            <th></th>
                        </tr>
                        <?$i = 1;?>
                        <? foreach ($dataProvider->models as $model):
                            /**
                             * @var $model \app\models\Category
                             */
                            ?>
                            <tr>
                                <td><?=$i++?></td>
                                <td><?=$model->title?></td>
                                <td width="100px">
                                    <div class="btn-group">
                                        <a class="btn btn-success" href="<?=\yii\helpers\Url::to('/journal/list?group='.$id."&subject=".$model->id)?>"><i class="fa fa-eye"></i></a>
                                    </div>
                                </td>
                            </tr>
                        <?endforeach;?>
                        <?if(count($dataProvider->models)<1):?>
                            <tr>
                                <td colspan="5">
                                    <?=Yii::t('app','Data not found')?>
                                </td>
                            </tr>
                        <?endif;?>
                        </tbody>
                    </table>
                    <div class="box-footer clearfix">
                        <?=\yii\widgets\LinkPager::widget(['pagination' => $dataProvider->pagination,'options'=>['class'=>'pagination pagination-sm no-margin pull-right'],'activePageCssClass' => 'green-active',])?>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>

