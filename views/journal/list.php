<?php
/**
 * Created by PhpStorm.
 * User: Sanjar
 * Date: 19.01.2020
 * Time: 20:01
 */

$this->title = Yii::t('app', 'Journal');
$this->params['breadcrumbs'][] = $this->title;
$count = 2;
?>


<style>
    .price{
        width: 100%;
    }

    td > input{
        border: none !important;
    }
    td{
        white-space: nowrap !important;
    }
    th{
        white-space: nowrap !important;
    }
    th.date{
        width: 90px;
    }
    .red{
        color: #f39c12;
    }

</style>
<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?=$this->title." | ".(\app\models\UserGroup::findOne($group)->title)." | ". (\app\models\Subject::findOne($subject)->title)?>
            </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="overflow-x: scroll">
            <? \yii\widgets\ActiveForm::begin(['action' => \yii\helpers\Url::to('/journal/price?group='.$group."&subject=".$subject), 'method' => 'POST'])?>
            <table class="table table-bordered" style="">
                <thead>
                    <th>
                        <?=Yii::t('app','Student')?>
                    </th>
                    <?foreach ($lesson_dates as $d):?>
                        <th class="date <?=$d == strtotime(date('d.m.Y'))?"red":""?>"><span><?=date('d.m',$d); $count++?><span class="no-print"><?=date('.Y',$d)?></span></span> </th>
                    <?endforeach;?>
                    <th class="no-print">
                        <?=Yii::t('app','Number of missed lesson')?>
                    </th>
                </thead>
                <tbody>
                <?foreach ($students as $student):
                    $missed = 0;
                    ?>
                    <tr>
                        <td><?=$student['fio']?></td>
                        <?foreach ($lesson_dates as $d):?>
                            <td class="price-td">
                                <?if($d <= strtotime(date('d.m.Y'))):?>
                                <input type="text" class="form-control price" name="price[<?=$student['id']?>][<?=$d?>]" value="<?=(isset($prices[$student['id']][$d]))?$prices[$student['id']][$d]:""?>">
                                <?if(isset($prices[$student['id']][$d]) && !is_numeric($prices[$student['id']][$d])) $missed++?>
                                <?endif;?>
                            </td>
                        <?endforeach;?>
                        <td class="price-td">
                            <?=$missed?>
                        </td>
                    </tr>
                <?endforeach;?>
                <tr class="no-print">
                    <td colspan="<?=$count?>">
                        <button class="btn btn-success pull-left">
                            <?=Yii::t('app','Submit')?>
                        </button>
                    </td>
                </tr>
                </tbody>

            </table>
            <? \yii\widgets\ActiveForm::end()?>
        </div>
    </div>
    <!-- /.box -->


</div>
