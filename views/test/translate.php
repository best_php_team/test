<?php

/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 13.01.2020
 * Time: 1:19
 */


/* @var $this \yii\web\View
 * @var $question \app\models\Questions
 * @var $answer \app\models\Answers
 */
$this->title = Yii::t('app', 'Test create');
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#uz" data-toggle="tab">Uzbek</a>
                    </li>
                    <li>
                        <a href="#ru" data-toggle="tab">Russian</a>
                    </li>
                    <li>
                        <a href="#en" data-toggle="tab">English</a>
                    </li>
                </ul>
                <div class="tab-content">


                    <!-- uz-->
                    <div class="tab-pane active" id="uz">
                        uz
                        <?=$this->render('form/_form',[
                                'lang'=>'uz',
                                'answer'=>$answer,
                                'question'=>$question
                        ])?>
                    </div>
                    <!-- /uz -->

                    <!-- ru-->
                    <div class="tab-pane" id="ru">
                        ru
                        <?=$this->render('form/_form',[
                            'lang'=>'ru',
                            'answer'=>$answer,
                            'question'=>$question
                        ])?>
                    </div>
                    <!-- ru -->
                    <!-- en-->
                    <div class="tab-pane" id="en">
                        en
                        <?=$this->render('form/_form',[
                            'lang'=>'en',
                            'answer'=>$answer,
                            'question'=>$question
                        ])?>
                    </div>
                    <!-- en -->

                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
