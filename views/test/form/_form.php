<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 13.01.2020
 * Time: 2:04
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $lang \app\models\Lang
 */
?>

<style>
    .labels {
        width: 5%;
    }
</style>
<?php $form = ActiveForm::begin([
    'options' => [
        'class' => ' '
    ]
]); ?>
<fieldset name="Answer[isright]">
<div class="row">
    <div class="col-md-10">
        <?= $form->field($question, 'question')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-2" >
        <?= $form->field($question, 'lang')->textInput(['value' => $lang]) ?>
    </div>
</div>
<hr>
    <div id="answers">

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($answer, 'answer[0]')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($answer, 'label[0]')->textInput([]) ?>
            </div>
            <div class="col-md-2">

                <!--        --><?//= $form->field($answer, 'isright')->radio(['class'=>'flat-red', 'value'=>'email', 'id'=>"isright0", 'checked'=>true]) ?>

                <label for="" style="margin-top: 29px">
                    <input type="radio" name="Answer[isright]" value="0" class="flat-red">
                    <?=$answer->getAttributeLabel('isright')?>
                </label>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <?= $form->field($answer, 'answer[1]')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($answer, 'label[1]')->textInput([]) ?>
            </div>
            <div class="col-md-2">

                <!--        --><?//= $form->field($answer, 'isright')->radio(['class'=>'flat-red', 'value'=>'phone', 'id'=>"isright1"]) ?>

                <label for="" style="margin-top: 29px">
                    <input type="radio" name="Answer[isright]" value="1" class="flat-red">
                    <?=$answer->getAttributeLabel('isright')?>
                </label>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($answer, 'answer[2]')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($answer, 'label[2]')->textInput([]) ?>
            </div>
            <div class="col-md-2">

                <!--        --><?//= $form->field($answer, 'isright')->radio(['class'=>'flat-red', 'value'=>'address', 'id'=>"isright2"]) ?>

                <label for="" style="margin-top: 29px">
                    <input type="radio" name="Answer[isright]" value="2" class="flat-red">
                    <?=$answer->getAttributeLabel('isright')?>
                </label>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <?= $form->field($answer, 'answer[3]')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($answer, 'label[3]')->textInput([]) ?>
            </div>
            <div class="col-md-2">

                <!--        --><?//= $form->field($answer, 'isright')->radio(['class'=>'flat-red', 'value'=>'address', 'id'=>"isright2"]) ?>

                <label for="" style="margin-top: 29px">
                    <input type="radio" name="Answer[isright]" value="3" class="flat-red">
                    <?=$answer->getAttributeLabel('isright')?>
                </label>
            </div>
        </div>

    </div>
<div class="form-group">
    <?= Html::button(Yii::t('app', 'Add new Answer row'), ['class' => 'btn btn-success', 'type'=>'button', 'id'=>'addAnswerButton']) ?>
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>
</fieldset>
<?php ActiveForm::end(); ?>

<?

$this->registerJs("
var temp = 4;
$('input[type=\"checkbox\"].flat-red, input[type=\"radio\"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
    $('#addAnswerButton').click(function(){
        $.post('".\yii\helpers\Url::to('/test/answer-row')."',{num:temp},function(e){
            $('#answers').append(e)
        })
        temp++;
    })
")

?>
