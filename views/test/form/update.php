<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 13.01.2020
 * Time: 2:43
 */

 use yii\helpers\Html;
 use yii\bootstrap\ActiveForm;
 /**
  * @var $lang \app\models\Lang
  */
 ?>

 <style>
     .labels {
         width: 5%;
     }
 </style>
<div class="margin-bottom">
    <?= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $question->id], ['class' => 'btn btn-primary']) ?>
</div>
 <?php $form = ActiveForm::begin([
     'options' => [
         'class' => ' '
     ]
 ]); ?>

 <?= $form->field($question, 'question')->textInput(['maxlength' => true]) ?>
 <?= $form->field($question, 'lang')->textInput(['value' => $lang]) ?>
 <hr>
 <?php
     foreach ($answer as $ans){

         ?>
 <?= $form->field($ans, 'id[0]')->textInput(['value' => $ans->id]) ?>
 <?= $form->field($ans, 'answer[0]')->textInput(['value' => $ans->answer]) ?>
 <?= $form->field($ans, 'label[0]')->textInput(['value' => $ans->label,'class' => 'labels']) ?>
 <?= $form->field($ans, 'isright')->radio(['value' => $ans->isright]) ?>
 <? } ?>
 <div class="form-group">
     <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
 </div>

 <?php ActiveForm::end(); ?>
