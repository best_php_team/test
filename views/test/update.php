<?php

/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 13.01.2020
 * Time: 2:34
 */

/* @var $this \yii\web\View */
/* @var $answer \app\models\Answers */
/* @var $question \app\models\Questions */
$this->title = Yii::t('app', 'Update Test');
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="box">
    <div class="box-body">
        <?= $this->render('form/update', [
            'lang' => $question->lang,
            'answer' => $answer,
            'question' => $question
        ]) ?>
    </div>
</div>
