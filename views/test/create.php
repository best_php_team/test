<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 13.01.2020
 * Time: 2:20
 */

/**
 * @var $this \yii\web\View
 * @var $question \app\models\Questions
 * @var $answer \app\models\Answers
 */

$this->title = Yii::t('app', 'Test create');
$this->params['breadcrumbs'][] = $this->title;
$lang = \app\models\Lang::getDefaultLang();
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('form/_form', [
            'lang' => $lang->url,
            'answer' => $answer,
            'question' => $question
        ]) ?>
    </div>
</div>