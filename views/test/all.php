<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 13.01.2020
 * Time: 1:49
 */

/* @var $this \yii\web\View */
$this->title = Yii::t('app', 'All Tests');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="margin-bottom">
    <?= Html::a(Yii::t('app', 'Create Acoount'), ['create'], ['class' => 'btn   btn-success  ']) ?>
</div>
<?php Pjax::begin(); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"></h3>

                <div class="box-tools">
                    <? $form = \yii\widgets\ActiveForm::begin(['action' => '/test/all', 'method' => 'get']) ?>
                    <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                        <input type="text" name="title" class="form-control pull-right"
                               value="<?= $searchModel->question ?>" placeholder="<?= Yii::t('app', 'Search') ?>">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <? \yii\widgets\ActiveForm::end() ?>
                </div>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>№</th>
                        <th><?= $searchModel->getAttributeLabel('question') ?></th>
                        <th><?= $searchModel->getAttributeLabel('lang') ?></th>
                        <th><?= $searchModel->getAttributeLabel('updated_at') ?></th>
                        <th><?= $searchModel->getAttributeLabel('created_at') ?></th>
                        <th><?= Yii::t('app','Actions') ?></th>
                        <th></th>
                    </tr>
                    <? $i = 1; ?>
                    <? foreach ($dataProvider->models as $model):
                        /**
                         * @var $model \app\models\Category
                         */
                        ?>
                        <tr>
                            <td><?= $i++ ?></td>
                            <td><?= $model->question ?></td>
                            <td><?= $model->lang ?></td>
                            <td><?= $model->updated_at ?></td>
                            <td><?= $model->created_at ?></td>
                            <td  >
                                <div class="btn-group">
                                    <a class="btn btn-warning"
                                       href="<?= \yii\helpers\Url::to('/test/update/' . $model->id) ?>"><i
                                            class="glyphicon glyphicon-pencil"></i></a>
                                    <a class="btn btn-info"
                                       href="<?= \yii\helpers\Url::to('/test/view/' . $model->id) ?>">
                                        <i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a class="btn btn-danger"
                                       data-confirm="<?= Yii::t('app', 'Are you sure you want to delete this item?') ?>"
                                       data-method="post"
                                       href="<?= \yii\helpers\Url::to('/test/delete/' . $model->id) ?>"><i
                                            class="glyphicon glyphicon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    <? if (count($dataProvider->models) < 1): ?>
                        <tr>
                            <td colspan="5">
                                <?= Yii::t('app', 'Data not found') ?>
                            </td>
                        </tr>
                    <? endif; ?>
                    </tbody>
                </table>
                <div class="box-footer clearfix">
                    <?= \yii\widgets\LinkPager::widget(['pagination' => $dataProvider->pagination, 'options' => ['class' => 'pagination pagination-sm no-margin pull-right'], 'activePageCssClass' => 'green-active',]) ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<?php Pjax::end(); ?>

