<?php

/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 13.01.2020
 * Time: 0:58
 */

/* @var $this \yii\web\View */
$this->title = Yii::t('app', 'Test');
$this->params['breadcrumbs'][] = $this->title;
$lessons[0]['fullname'] = 'Fullname';
$lessons[0]['date'] = time();
$lessons[0]['id'] = 1;
$students = \app\models\User::find()->all();
?>
<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Bordered Table</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">

                <?= \app\widgets\journal\Journal::widget([
                    'students' => $students,
                    'lessons' => $lessons,
                    'options' => [
                        'url' => 'url/to/save',
                        'date' => 'd.m.Y'
                    ]
                ]) ?>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li>
            </ul>
        </div>
    </div>
    <!-- /.box -->


</div>

