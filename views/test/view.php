<?php

use yii\helpers\Html;

/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 13.01.2020
 * Time: 2:50
 */

/* @var $this \yii\web\View */
/* @var $answer static[] */
/* @var $question null|static */
$this->title = Yii::t('app', 'View Test');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="margin-bottom">
    <?= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $question->id], ['class' => 'btn btn-primary']) ?>
</div>
<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Bordered Table</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <?= \yii\widgets\DetailView::widget([
                'model' => $question,
                'attributes' => [
                    'question',
                    'lang',
                    'created_at:date',
                    'updated_at:date',
                ],
            ]) ?>

            <? foreach ($answer as $item) {
                echo \yii\widgets\DetailView::widget([
                    'model' => $item,
                    'attributes' => [
                        'answer',
                        'label',
                        'isright',
                    ],
                ]);
            } ?>
        </div>
    </div>
</div>
