<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->firstname;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<h1><?= \yii\bootstrap\Html::encode($this->title) ?></h1>
<div class="margin-bottom">

    <?= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
    </p>
    <div class="col s12 m12 l6">
        <div class="card">
            <div class="card-content">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'firstname',
                        'lastname',
                        'middlename',
                        'created_at:date',
                        'updated_at:date',
                        [
                            'attribute' => 'status',
                            'value' => function ($model) {
                                return $model->status;
                            },
                            'format' => 'html'
                        ],
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>
