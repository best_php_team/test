<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'middlename')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'login')  ->textInput([
                                            'autofocus' => true,
                                            'tabindex' => 2,
                                        ]) ?>

    <?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true, 'value'=>'']) ?>

    <?=$form->field($model, 'user_group')->dropDownList(\app\models\UserGroup::dropList(), ['class'=>'form-control select2'])?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
