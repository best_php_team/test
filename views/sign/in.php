<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t("app", 'Sign In');

?>
<div class="login-box"  >
    <div class="login-logo">
        <a href="<?=Url::to("/")?>"><b>Test</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?=Yii::t('app','Sign in to start your session')?></p>

        <?php $form = ActiveForm::begin()?>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'login',['template'=>'{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span>'])->textInput(['autofocus' => true,'placeholder'=>Yii::t('app','Login')]) ?>
            </div>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'password',['template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>'])->passwordInput([ 'placeholder'=>Yii::t('app','Password')]) ?>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <?= $form->field($model, 'rememberMe',['template'=>'<label>{input} {label}</label>'])->checkbox()?>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat"><?=Yii::t('app','Sign In')?></button>
                </div>
                <!-- /.col -->
            </div>
        <?php ActiveForm::end(); ?>
        <a href="#"><?=Yii::t('app','I forgot my password')?></a><br>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
