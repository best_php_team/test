<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t("app", 'Sign up');

?>
<span class="card-title"><?= Yii::t("app", 'Sign up') ?></span>
<div class="row">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'login')
        ->textInput([
            'autofocus' => true,
            'tabindex' => 2,
            'class' => ' required ',
        ]) ?>

    <?= $form->field($model, 'password')
        ->passwordInput([
            'tabindex' => 4
        ]) ?>

    <?= Html::submitButton(Yii::t('app', 'Sign up'), ['class' => 'btn-block waves-effect waves-light btn teal', 'tabindex' => 5]); ?>
    <p>
        <a href="<?= \yii\helpers\Url::to('/sign/in') ?>"
        ><?= Yii::t('app', 'Login') ?></a>
    </p>

    <?php ActiveForm::end(); ?>
</div>
