<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 07.07.2019
 * Time: 17:19
 * @var $controller Yii::$app->controller->id
 * @var $action Yii::$app->controller->action->id
 * @var $user  Yii::$app->user->identity
 */

use yii\helpers\Url;
$checkUser = Yii::$app->user;
?>


<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/web/css/dist/img/default-photo.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= $user->fio ?></p>
                <a><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                      <i class="fa fa-search"></i>
                    </button>
                  </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header"><?= Yii::t('app', 'MAIN NAVIGATION') ?></li>
            <? if ($checkUser->can("DASHBOARD")) { ?>
                <li class="<?= ($controller == 'site' && $action == 'index') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/') ?>">
                        <i class="fa fa-home"></i> <span><?= Yii::t('app', 'Dashboard') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("Journal-index")) { ?>
                <li class="<?= ($controller == 'site' && $action == 'index') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/journal/index') ?>">
                        <i class="fa fa-file-text-o"></i> <span><?= Yii::t('app', 'Journal') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("Test-all")) { ?>
                <li class="<?= ($controller == 'site' && $action == 'index') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/test/all') ?>">
                        <i class="fa  fa-tasks"></i> <span><?= Yii::t('app', 'All tests') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("Test-pass")) { ?>
                <li class="<?= ($controller == 'site' && $action == 'index') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/test/pass') ?>">
                        <i class="fa  fa-tasks"></i> <span><?= Yii::t('app', 'Pass the test') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("Test-create")) { ?>
                <li class="<?= ($controller == 'site' && $action == 'index') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/test/create') ?>">
                        <i class="fa  fa-tasks"></i> <span><?= Yii::t('app', 'Create test') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("Configurations-index")) { ?>
                <li class="<?= ($controller == 'configurations' && $action == 'index') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/configurations/index') ?>">
                        <i class="fa  fa-tasks"></i> <span><?= Yii::t('app', 'Configurations') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("Category-index")) { ?>
                <li class="<?= ($controller == 'category') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/category/index') ?>">
                        <i class="fa fa-truck" style="position:relative; color: #000;">
                            <i style="position: absolute; right: 3px; bottom: 0; color: #f39c12; font-size: 65%"
                               class="fa fa-car"></i>
                        </i>
                        <span><?= Yii::t('app', 'Categories') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("Subject-index")) { ?>
                <li class="<?= ($controller == 'subject') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/subject/index') ?>">
                        <i class="fa fa-book" style="position:relative; color: #000;"></i>
                        <span><?= Yii::t('app', 'Subjects') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("UserGroup-index")) { ?>
                <li class="<?= ($controller == 'user-group') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/user-group/index') ?>">
                        <i class="fa fa-group"></i>
                        <span><?= Yii::t('app', 'Groups') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("User-index")) { ?>
                <li class="<?= ($controller == 'user' && $action == 'about') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/user/index') ?>">
                        <i class="fa fa-user"></i>
                        <span><?= Yii::t('app', 'Users') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("DEVELOPER")) { ?>
                <li class="<?= ($controller == 'icons' && $action == 'contact') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/site/icons') ?>">
                        <i class="fa fa-circle-o text-yellow"></i>
                        <span><?= Yii::t('app', 'Icons') ?></span>
                    </a>
                </li>
            <? } ?>
            <? if ($checkUser->can("Auth-index")) { ?>
                <li class="<?= ($controller == 'icons' && $action == 'contact') ? 'active' : "" ?>">
                    <a href="<?= Url::to('/auth/default/index') ?>">
                        <i class="fa fa-circle-o text-yellow"></i>
                        <span><?= Yii::t('app', 'Auth') ?></span>
                    </a>
                </li>
            <? } ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
