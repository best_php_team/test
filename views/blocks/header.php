<?php
use yii\helpers\Url;
echo \app\widgets\Toastr::widget();
?>
<header class="main-header">

    <!-- Logo -->
    <a href="<?=Url::to('/')?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b style="color: #f39c12">G-T</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b style="color: #f39c12">G-T</b>EST</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/web/css/dist/img/default-photo.png" class="img-circle" style="height: 18px" alt="User Image">
                        <span class="hidden-xs"><?=$user->fio?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/web/css/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                            <p>
                                <?=$user->login?> - Web Developer
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?=Url::to('/profile')?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?=Url::to('/sign/out')?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>

    </nav>
</header>