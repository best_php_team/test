<?php

use yii\helpers\Url;

$action = $this->context->action->id;
?>
<style>
    .list-group-horizontal .list-group-item {
        display: inline-block;
    }

    .list-group-horizontal .list-group-item {
        margin-bottom: 0;
        margin-left: -4px;
        margin-right: 0;
        border-right-width: 0;
    }

    .list-group-horizontal .list-group-item:first-child {
        border-top-right-radius: 0;
        border-bottom-left-radius: 4px;
    }

    .list-group-horizontal .list-group-item:last-child {
        border-top-right-radius: 4px;
        border-bottom-left-radius: 0;
        border-right-width: 1px;
    }
    .list-group-item.active > a, .list-group-item > a{
        color: white;
    }

</style>

<form class="form-inline">
    <ul class="list-group list-group-horizontal">
        <li class="list-group-item  bg-aqua <?= ($action === 'index') ? ' active ' : '' ?>">
            <a    href="<?= Url::to('/translation/index') ?>">
                <?= Yii::t('app', 'Translations') ?>
            </a>
        <li class="list-group-item bg-aqua">
            <a href="<?= Url::to('/translation/create') ?>">
                <?= Yii::t('app', 'Add word') ?></a>
        </li>

        <li class="list-group-item bg-aqua">
            <a href="<?= Url::to('/translation/language') ?>"><?= Yii::t('app', 'Languages') ?></a>
        </li>
    </ul>
</form>
