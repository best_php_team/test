<?php

$languages = app\models\Lang::find()->all();
$this->title = Yii::t('app', 'Add translation');

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <?= \yii\bootstrap\Html::encode($this->title) ?>
            </div>
        </div>
    </div>

    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">

                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'language')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\Lang::find()->all(),'url','name')) ?>

                <?= $form->field($model, 'id')->hiddenInput(['value'=> $id])->label(false) ?>
                <?= $form->field($model, 'translation')->textInput() ?>

                <div class="btn-group">
                    <button type="submit" class="btn-block waves-effect waves-light btn teal">
                        <?= Yii::t('app', "Save") ?>
                    </button>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>