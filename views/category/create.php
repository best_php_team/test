<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = Yii::t('app', 'Create Category');
$this->params['breadcrumbs'][] = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body">
                        <?= $form->field($model, 'title',['template'=>'<div class="form-group">{label}{input}<div class="col-md-12">{error}</div></div>'])->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="box-footer">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>
