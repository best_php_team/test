<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Configurations */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<p>
    <?= Html::a(Yii::t("app", "Back"), ['index'], ['class' => 'btn btn-primary']);
    ?>
    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</p>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h1 class="box-title margin-bottom "><?= Html::encode($this->title) ?></h1>
                <div class="box-body table-responsive ">

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'title',
                            'value',
                            'created_at:date',
                            'updated_at:date',
                        ],
                    ]) ?>

                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </div>
</div>

