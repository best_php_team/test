<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Configurations */

$this->title = Yii::t('app', 'Create Configurations');
$this->params['breadcrumbs'][] = $this->title;
echo Html::a(Yii::t("app", "Back"), ['index'], ['class' => 'btn btn-primary margin-bottom']);
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <b><?= $this->title ?></b>
                </h3>
                <div class="box-body table-responsive ">
                    <?= $this->render("form/_form", ['model' => $model]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
