<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConfigurationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Configurations');
$this->params['breadcrumbs'][] = $this->title;


echo Html::a(Yii::t("app", "Create configuration"), ['create'], ['class' => 'btn btn-primary margin-bottom margin-r-5']);

?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h1 class="box-title margin-bottom "><?= Html::encode($this->title) ?></h1>
                <div class="box-body table-responsive ">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'title',
                            'value',
                            'created_at:date',
                            'updated_at:date',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </div>
</div>
