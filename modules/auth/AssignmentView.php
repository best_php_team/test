<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 22:26
 */

namespace app\modules\auth;


use yii\base\Action;
use yii\data\ActiveDataProvider;

class AssignmentView extends Action
{
    public $layout;

    public $defaultView = "view";

    public $params = [];

    public function run()
    {
        $this->controller->layout = $this->layout;
        $provider = new ActiveDataProvider([
            'query' => $this->params['model']::find()->where(['!=','item_name','ROOT']),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->controller->render("index", ['dataProvider' => $provider]);
    }
}