<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 18:57
 */

namespace app\modules\auth;

use Yii;
use yii\base\BootstrapInterface;
use yii\helpers\Json;
use yii\rbac\Item;
use yii\web\ForbiddenHttpException;

/**
 * This is the main module class for the Auth module.
 *
 * To use Auth, include it as a module in the application configuration like the following:
 *
 * ~~~
 * return [
 *     'bootstrap' => ['auth'],
 *     'modules' => [
 *         'auth' => ['class' => 'vendor\auth\Auth'],
 *     ],
 * ]
 * ~~~
 *
 */
class Auth extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\auth\controllers';

    public $layout = "/main";

    public $models = [];

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        if ($app instanceof \yii\web\Application) {
            $app->getUrlManager()->addRules([
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>/<action:[\w\-]+>/<parent:[\w\-]+>/<child:[\w\-]+>', 'route' => $this->id . '/<controller>/<action>'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id, 'route' => $this->id . '/default/index'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<id:\w+>', 'route' => $this->id . '/default/view'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>/<action:[\w\-]+>', 'route' => $this->id . '/<controller>/<action>'],
            ], false);
        }
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeAction($action)
    {
        foreach (array_merge($this->coreModels(), $this->models) as $id => $config) {
            if (is_object($config)) {
                $this->models[$id] = $config;
            } else {
                $this->models[$id] = Yii::createObject($config);
            }
        }
        $this->resetGlobalSettings();
        return true;
    }

    /**
     * Returns the list of the core code generator configurations.
     * @return array the list of the core code generator configurations.
     */
    public function coreModels()
    {
        return [
            'permission' => ['class' => 'app\modules\auth\models\AuthItem','type'=>Item::TYPE_PERMISSION],
            'role' => ['class' => 'app\modules\auth\models\AuthItem','type'=>Item::TYPE_ROLE],
            'rule' => ['class' => 'app\modules\auth\models\AuthRule'],
            'item-child' => ['class' => 'app\modules\auth\models\AuthItemChild'],
            'assignment' => ['class' => 'app\modules\auth\models\AuthAssignment'],
        ];
    }

    public function getName()
    {
        return "Auth module";
    }

    /**
     * Resets potentially incompatible global settings done in app config.
     */
    protected function resetGlobalSettings()
    {
        if (Yii::$app instanceof \yii\web\Application) {
            Yii::$app->assetManager->bundles = [];
        }
    }

}
