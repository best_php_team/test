<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:38
 */

namespace app\modules\auth\controllers;

use app\modules\auth\models\AuthItemChild;
use app\modules\auth\models\AuthRule;
use app\modules\auth\ViewAction;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ErrorAction;
use Yii;
use yii\web\UnsupportedMediaTypeHttpException;

class ItemChildController extends Controller
{

    public function actions()
    {
        return [
            'error' => ['class' => ErrorAction::className()],
            'index' => ['class' => ViewAction::className(),
                'params'=>['model' => AuthItemChild::className()],
            ],
        ];
    }


    public function actionCreate()
    {
        $model = new \app\modules\auth\models\AuthItemChild();

        if(Yii::$app->request->post()){
            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate()  && $model->save()) {
                    return  $this->redirect("index");
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionView($parent,$child){
        $authManager = Yii::$app->getAuthManager();
        if($authManager instanceof  yii\rbac\DbManager){
            $query = (new Query())->from($authManager->itemChildTable)
                ->where(['parent'=>$parent,'child'=>$child])
                ->one($authManager->db);
            return $this->render('view',['model'=>$query]);
        }
        else
            throw new UnsupportedMediaTypeHttpException("This function not support type of ". $authManager::className());

    }

    public function actionDelete($parent, $child){
        $authManager = Yii::$app->getAuthManager();
        if($authManager instanceof  yii\rbac\DbManager){
            $parent = $authManager->getRole($parent);
            $child = $authManager->getPermission($child);
            $authManager->removeChild($parent,$child);
            return $this->redirect("/auth/item-child/index");
        }
        else
            throw new UnsupportedMediaTypeHttpException("This function not support type of ". $authManager::className());

    }


}