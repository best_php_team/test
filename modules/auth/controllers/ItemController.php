<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 19:49
 */

namespace app\modules\auth\controllers;


use app\modules\auth\AuthManager;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Url;
use yii\rbac\Item;
use yii\rbac\Permission;
use yii\rbac\Role;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\NotAcceptableHttpException;

class ItemController extends Controller
{
    /**
     * @var $type
     * @see \yii\rbac\Item::$type
     */
    protected $type;

    /**
     * @return array
     * @see ErrorAction
     */
    public function actions()
    {
        return [
            'error' => ['class' => ErrorAction::className()],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $manager = \Yii::$app->authManager;
        if ($this->type === Item::TYPE_PERMISSION) {
            $query = (new Query())
                ->from($manager->itemTable)
                ->where(['type' => Item::TYPE_PERMISSION]);
        } else {
            $query = (new Query())
                ->from($manager->itemTable)
                ->where(['type' => Item::TYPE_ROLE]);
        }
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render("index", ['dataProvider' => $provider]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws Exception
     * @throws NotSupportedException
     */
    public function actionCreate()
    {
        $manager = \Yii::$app->authManager;
        $model = new Item();
        $item = '';
        if ($post = \Yii::$app->request->post()) {
            if ($this->type === Item::TYPE_PERMISSION) {
                $item = new Permission();
                $item->name = $post['name'];
            } else if ($this->type === Item::TYPE_ROLE) {
                $item = new Role();
                $item->name = $post['name'];
            } else throw new NotSupportedException($post['type'] . " Not Found", 409);

            $item->description = $post['description'];
            //$item->data = $post['data'];
            if ($manager->add($item)) {
                return $this->redirect("index");
            }
            throw new Exception("Item not saved", 409);
        }
        return $this->render("create", ['model' => $model]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws Exception
     * @throws NotSupportedException
     */
    public function actionUpdate($name)
    {
        $manager = \Yii::$app->authManager;
        if ($this->type === Item::TYPE_PERMISSION) {
            $item = $manager->getPermission($name);
        } else if ($this->type === Item::TYPE_ROLE) {
            $item = $manager->getRole($name);
        } else throw new NotSupportedException($name . " Role not Found", 409);

        if ($post = \Yii::$app->request->post()) {
            $item->description = $post['description'];
            $item->name = $post['name'];
           // $item->data = $post['data'];
            if ($manager->update($name, $item)) {
                return $this->redirect(Url::to("index"));
            }
            throw new Exception("Item not saved", 409);
        }
        return $this->render("create", ['model' => $item]);
    }

    public function actionDelete($name)
    {
        $manager = \Yii::$app->authManager;

        if ($this->type === Item::TYPE_PERMISSION) {
            $item = $manager->getPermission($name);
        } else if ($this->type === Item::TYPE_ROLE) {
            $item = $manager->getRole($name);
        }
        if ($manager->remove($item)) {
            return $this->redirect("index");
        }
    }

    /**
     * @param $name
     * @throws NotSupportedException
     */
    public function actionView($name)
    {
        //TODO Create view action
        throw new NotAcceptableHttpException("Now view action not supported", 406);
    }

}
