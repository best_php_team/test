<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 29.09.2019
 * Time: 20:34
 */

namespace app\modules\auth\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\rbac\Item;

class RoleController extends ItemController
{
    public $type = Item::TYPE_ROLE;

    public function actionView($name)
    {
        $manager = Yii::$app->authManager;

        $childrenList = $this->getChildrenList($manager);
        $result = [];
        $this->getChildrenRecursive($name, $childrenList, $result);
        if (empty($result)) {
            return [];
        }
        $query = (new Query())->from($manager->itemTable)->where([
            'type' => Item::TYPE_PERMISSION,
            'name' => array_keys($result),
        ]);
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render("view", ['dataProvider' => $provider,'parent'=>$name]);
    }
    public function getPermissionsByRole($roleName)
    {
        $childrenList = $this->getChildrenList();
        $result = [];
        $this->getChildrenRecursive($roleName, $childrenList, $result);
        if (empty($result)) {
            return [];
        }
        $query = (new Query())->from($this->itemTable)->where([
            'type' => Item::TYPE_PERMISSION,
            'name' => array_keys($result),
        ]);
        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $row['name'];
        }

        return $permissions;
    }
    protected function getChildrenList($manager)
    {
        $query = (new Query())->from($manager->itemChildTable);
        $parents = [];
        foreach ($query->all($manager->db) as $row) {
            $parents[$row['parent']][] = $row['child'];
        }

        return $parents;
    }
    protected function getChildrenRecursive($name, $childrenList, &$result)
    {
        if (isset($childrenList[$name])) {
            foreach ($childrenList[$name] as $child) {
                $result[$child] = true;
                $this->getChildrenRecursive($child, $childrenList, $result);
            }
        }
    }
}