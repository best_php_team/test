<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\auth\controllers;

use app\modules\auth\Auth;
use yii\web\Controller;

class DefaultController extends Controller
{
    public $layout = 'main';

    /**
     * @var Auth
     */
    public $module;

    public function actionIndex()
    {
        return $this->render('index');
    }

}
