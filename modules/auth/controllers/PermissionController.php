<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 29.09.2019
 * Time: 20:33
 */

namespace app\modules\auth\controllers;


use yii\rbac\Item;

class PermissionController extends ItemController
{
    public $type = Item::TYPE_PERMISSION;
}