<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:38
 */

namespace app\modules\auth\controllers;

use app\modules\auth\AssignmentView;
use app\modules\auth\models\AuthAssignment;
use Yii;
use yii\web\Controller;
use yii\web\ErrorAction;

class AssignmentController extends Controller
{
    public function actions()
    {
        return [
            'error' => ['class' => ErrorAction::className()],
            'index' => ['class' => AssignmentView::className(),
                'params'=>['model' => AuthAssignment::className()],
            ],
        ];
    }



    public function actionCreate()
    {
        $model = new \app\modules\auth\models\AuthAssignment();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()  && $model->save()) {
                return  $this->redirect("index");
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($item_name, $user_id){
        $manager = Yii::$app->authManager->getRolesByUser($user_id);

        $user = Yii::$app->user->identity;

        return $this->render('view',['model'=>$manager]);
    }

    public function actionDelete($item_name,$user_id){
        $manager = Yii::$app->authManager->revoke($item_name, $user_id);
        return $this->redirect("/auth/assignment/index");
    }

}