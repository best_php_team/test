<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\auth\models\AuthItem;
use yii\helpers\ArrayHelper;
use yii\rbac\Permission;
/* @var $this yii\web\View */
/* @var $model app\modules\auth\models\AuthItemChild */
/* @var $form ActiveForm */
?>
<div class="form-_form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent')->dropDownList([
            ArrayHelper::map(AuthItem::findAll(['type'=>Permission::TYPE_ROLE]),'name','name')
    ]) ?>

    <?= $form->field($model, 'child')->dropDownList([
        ArrayHelper::map(AuthItem::findAll(['type'=>Permission::TYPE_PERMISSION]),'name','name')
    ])  ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-_form -->
