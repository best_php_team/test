<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:41
 * @var $dataProvider
 */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Roles permissions');
$this->params['breadcrumbs'][] = $this->title;


echo Html::a(Yii::t("app", "Create"), ['create'], ['class' => 'btn btn-primary margin-bottom margin-r-5']);
echo Html::a(Yii::t('app', 'Back'), '/auth/default/index', ['class' => 'btn btn-primary margin-bottom margin-r-5']);

?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h1 class="box-title margin-bottom "><?= Html::encode($this->title) ?></h1>
                <div class="box-body table-responsive ">
                    <?=
                    \yii\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'tableOptions' => ['class' => 'table table-hover'],
                        'layout' => "{pager}\n{items}",
                        'columns' => [
                            ['class' => \yii\grid\SerialColumn::className()],
                            [
                                'attribute' => 'parent',
                                'format' => 'html',
                                'value' => function ($model) {
                                    return Html::a($model['parent'], ['/auth/role/view', 'name' => $model['parent']]);
                                },
                            ],
                            'child',
                            ['class' => \yii\grid\ActionColumn::className(),
                                'template' => '  {delete}',
                            ]
                        ]
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

