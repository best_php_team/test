<?php

use yii\helpers\Html;
use app\modules\auth\models\AuthRule;
?>
<div class="form-_form">

    <?= Html::beginForm('', 'post') ?>

    <?$input = Html::label('Nomi', 'authitem-name', null, ["class" => "form-control"]) ;
    $input .= Html::textInput('name', $model?$model->name:'', ['id' => 'authitem-v', "class" => "form-control"])?>
    <?=Html::tag('div',$input,['class'=>'form-group'])?>

    <?= Html::hiddenInput('type', \yii\rbac\Permission::TYPE_PERMISSION, ['id' => 'authitem-type', "class" => "form-control"]) ?>

    <?= Html::label('Description', 'authitem-description', null, ["class" => "form-control"]) ?>
    <?= Html::textInput('description', $model?$model->description:'', ['id' => 'authitem-description', "class" => "form-control margin-bottom"]) ?>

    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>

    <?= Html::endForm(); ?>

</div><!-- form-_form -->
