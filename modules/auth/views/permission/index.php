<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:41
 * @var $dataProvider
 */

use yii\rbac\Permission;
use yii\rbac\Item;
use yii\rbac\Role;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Permissions');
$this->params['breadcrumbs'][] = $this->title;

echo Html::a(Yii::t("app", "Create"), ['create'], ['class' => 'btn btn-primary margin-bottom margin-r-5']);
echo Html::a(Yii::t('app', 'Back'), '/auth/default/index', ['class' => 'btn btn-primary margin-bottom  ']);

?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h1 class="box-title margin-bottom "><?= Html::encode($this->title) ?></h1>
                <div class="box-body table-responsive ">

                    <?php
                    echo
                    \yii\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'tableOptions' => ['class' => 'table table-hover'],
                        'layout' => "{pager}\n{items}",
                        'columns' => [
                            ['class' => \yii\grid\SerialColumn::className()],
                            ['label' => 'Name',
                                'value' => function ($model) {
                                    return $model['name'];
                                },
                            ],
                            [
                                'label' => 'Type',
                                'value' => function ($model) {
                                    return $model['type'] == Item::TYPE_PERMISSION ? Permission::className() : Role::className();
                                },
                            ],
                            [
                                'label' => 'description',
                                'value' => function ($model) {
                                    return $model['description'];
                                },
                            ],

                            [
                                'class' => ActionColumn::className(),
                                'template' => '{delete}',
                                'buttons' => [
                                    'delete' => function ($url, $model, $key) {
                                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-trash"]);
                                        return \yii\helpers\Html::a($icon, Url::to(['permission/delete', 'name' => $model['name']]));
                                    }
                                ],
                            ],

                        ]
                    ]);
                    ?>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </div>
</div>

