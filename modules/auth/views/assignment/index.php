<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:41
 * @var $dataProvider
 */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Assigned to users');
$this->params['breadcrumbs'][] = $this->title;

?>


<?= Html::a(Yii::t("app", "Create"), ['create'], ['class' => 'btn btn-primary margin-bottom margin-r-5']); ?>
<?= Html::a(Yii::t('app', 'Back'), '/auth/default/index', ['class' => 'btn btn-primary margin-bottom margin-r-5']); ?>


<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h1 class="box-title margin-bottom "><?= Html::encode($this->title) ?></h1>
                <div class="box-body table-responsive ">
                    <?= \yii\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'tableOptions' => ['class' => 'table table-hover'],
                        'layout' => "{pager}\n{items}",
                        'columns' => [
                           'item_name',
                            [
                                'attribute' => 'user_id',
                                'value' => function ($model) {
                                    return $model->username->fio;
                                }
                            ],
                            'created_at:datetime',
                            ['class' => \yii\grid\ActionColumn::className(),
                                'header' => 'Actions',
                                'template' => '{view} {delete}'
                            ]
                        ]
                    ]);

                    ?>
                </div>
            </div>
        </div>
    </div>
</div>