<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\auth\models\AuthItem;
use yii\rbac\Permission;

/* @var $this yii\web\View */
/* @var $model app\modules\auth\models\AuthAssignment */
/* @var $form ActiveForm */
$userModel = Yii::$app->user->identityClass;
?>
<div class="form-_form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'item_name')->dropDownList([
        ArrayHelper::map(AuthItem::findAll(['type' => Permission::TYPE_ROLE]), 'name', 'name')
    ]) ?>
    <?= $form->field($model, 'user_id')->dropDownList([
        ArrayHelper::map(\app\models\User::find()->all(), "id", 'email')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-_form -->
