<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 06.10.2019
 * Time: 23:19
 */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Assigned roles');
$this->params['breadcrumbs'][] = $this->title;
echo Html::a(Yii::t("app", "Back"), ['index'], ['class' => 'btn btn-primary margin-bottom margin-r-5']);
echo Html::a(Yii::t("app", "Create"), ['/auth/item-child/create'], ['class' => 'btn btn-primary margin-bottom margin-r-5']);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <b><?= $this->title ?></b>
                </h3>
                <div class="box-body table-responsive  ">
                    <table class="table table-hover">
                        <tr>
                            <th>№</th>
                            <th><?= Yii::t('app', 'Name') ?></th>
                            <th><?= Yii::t('app', 'description') ?></th>
                        </tr>
                        <tbody>
                        <?$i=0; foreach ($model as $model) { ?>
                            <tr>
                                <td><?=++$i?></td>
                                <td>
                                    <a href="<?= \yii\helpers\Url::to(['/auth/role/view/', 'name' => $model->name]) ?>">
                                        <?= $model->name ?></a>
                                </td>
                                <td><?= $model->description ?></td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
