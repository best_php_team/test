<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $models \yii\gii\model[] */
/* @var $content string */

$models = Yii::$app->controller->module->models;
$this->title = Yii::t("app",'Welcome to Auth');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="default-index">
    <div class="page-header">
        <small>Welcome to Auth  for all users roles and authorities</small>
    </div>
    <div class="row">
        <?php foreach ($models as $id => $model): if($model->getName()=="Auth rule")continue;?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"> <?= Html::encode($model->getName()) ?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?= $model->getDescription() ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <p><?= Html::a('Start &raquo;', [$id.'/index'], ['class' => 'btn btn-default btn-block']) ?></p>
                    </div>
                </div>
                <!-- /.box -->
            </div>

        <?php endforeach; ?>
    </div>


</div>
