<?php

use yii\helpers\Html;
use app\modules\auth\models\AuthRule;

?>
<div class="form-_form">

    <?= Html::beginForm('', 'post') ?>

    <?= Html::label('Nomi', 'authitem-name', null, ["class" => "form-control"]) ?>
    <?= Html::textInput('name', $model ? $model->name : '', ['id' => 'authitem-v', "class" => "form-control"]) ?>

    <?= Html::hiddenInput('type', \yii\rbac\Permission::TYPE_ROLE, ['id' => 'authitem-type', "class" => "form-control"]) ?>

    <?= Html::label('Description', 'authitem-description', null, ["class" => "form-control"]) ?>
    <?= Html::textInput('description', $model ? $model->description : '', ['id' => 'authitem-description', "class" => "form-control margin-bottom"]) ?>

    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>

    <?= Html::endForm(); ?>

</div><!-- form-_form -->
