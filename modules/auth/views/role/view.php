<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 06.10.2019
 * Time: 23:20
 * @var $dataProvider
 * @var $parent
 */

use yii\rbac\Permission;
use yii\rbac\Item;
use yii\rbac\Role;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'View role');
$this->params['breadcrumbs'][] = $this->title;
echo Html::a(Yii::t("app", "Back"), ['index'], ['class' => 'btn btn-primary margin-bottom margin-r-5']);
echo Html::a(Yii::t("app", "Create"), ['/auth/item-child/create'], ['class' => 'btn btn-primary margin-bottom margin-r-5']);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <b><?= $this->title ?></b>
                </h3>
                <div class="box-body table-responsive ">
                    <?= \yii\grid\GridView::widget([
                        'tableOptions' => ['class' => 'table table-hover'],
                        'layout' => "{pager}\n{items}",
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => \yii\grid\SerialColumn::className()],
                            ['label' => 'Name',
                                'value' => function ($model) {
                                    return $model['name'];
                                },
                            ],
                            [
                                'label' => 'Type',
                                'value' => function ($model) {
                                    return $model['type'] == Item::TYPE_PERMISSION ? Permission::className() : Role::className();
                                },
                            ],
                            [
                                'label' => 'description',
                                'value' => function ($model) {
                                    return $model['description'];
                                },
                            ],


                            [
                                'class' => ActionColumn::className(),
                                'template' => '{delete}',
                                'buttons' => [
                                    'delete' => function ($url, $model) use ($parent) {
                                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-trash"]);
                                        return \yii\helpers\Html::a($icon, Url::to(['role/delete', 'parent' => $parent, 'child' => $model['name']]));
                                    }
                                ],
                            ],

                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
