<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 11.12.2019
 * Time: 7:56
 */

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\widgets\ToastrAsset;

/**
 * echo \app\widgets\Toastr::widget();
 */
class Toastr extends Widget
{
    /**
     * @var array
     */
    public $options = [];

    public function init()
    {
        $view = Yii::$app->getView();
        ToastrAsset::register($view);
    }

    /**
     * @return bool
     */
    public function run()
    {

        $this->notifyMessages();
    }

    /**
     * @param array $messages
     */
    public function notifyMessages()
    {
        $action = \Yii::$app->view;
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
            $action->
            registerJs('
            toastr["'.$key.'"]("'.$message.'")
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": true,
              "positionClass": "toast-top-right",
              "preventDuplicates": false,
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
            }
        ');
        }
    }

    public function registerClientScripts()
    {
    }
}