<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 12.01.2020
 * Time: 0:02
 */

namespace app\widgets;


use yii\web\AssetBundle;

class ToastrAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'web/css/bower_components/toastr/toastr.min.css'
    ];
    public $js = [
        'web/css/bower_components/toastr/toastr.min.js'
    ];
}