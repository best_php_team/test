<?php

namespace app\widgets\journal;
use yii\helpers\Url;


/**
 *<?= \app\widgets\journal\Journal::widget([
 * 'students' => $students,
 * 'lessons' => $lessons,
 * 'options' => [
 * 'url' => 'url/to/save',
 * 'date' => 'd.m.Y'
 * ]
 * ]) ?>
 */
class Journal extends \yii\bootstrap\Widget
{
    public $students = [];
    public $lessons = [];

    public $inputOptions = [];
    public $headerOptions = [];

    private $url = "";
    private $date = "Y.m.d";

    private $priceInputs = "";

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getPriceInputs()
    {
        $this->setUrl(Url::to(['journal/widget']));
        if($this->options['url']){
            $this->setUrl($this->options['url']);
        }
        return $this->priceInputs;
    }

    /**
     * @param string $priceInputs
     */
    public function setPriceInputs($priceInputs)
    {
        $this->priceInputs = $priceInputs;
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if($this->options['date']){
            $this->date = $this->options['date'];
        }
        $this->checkRequirements();
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        echo $this->renderHeader($this->lessons);
        echo $this->renderBody($this->students, $this->lessons);
    }


    private function checkRequirements()
    {
    }

    private function renderHeader($lessons = [])
    {
        $i=0;
        $priceInputs = "";
        $header = "";
        $header .= "<thead>";
        $header .= "<tr>";
        $header .= "<th style='width: 10px'>#</th>";
        $header .= "<th>Name</th>";
        foreach ($lessons as $ls) {
            $header .= "<th> " . date($this->date,$ls['date']) . " </th>";
        }
        $header .= " </tr>";
        $header .= " </thead>";
        return $header;
    }

    private function renderBody($students = [], $lessons = [])
    {
        $body = "";

        $body.="<form>";
        $body .= " <tbody>";
        foreach ($students as $st) {
            $priceInputs = '';
            foreach ($lessons as $ls) {
                $priceInputs .= "<td><input";
                $priceInputs .= (date($this->date,$ls['date']) !== date($this->date,time())) ? " disabled ":" ";
                $priceInputs .= " class='price form-control' ";
                $priceInputs .= " type='text' ";
                $priceInputs .= " name='price[".$st['id']."][".$ls['id']."]' ";
                $priceInputs .= " value='" . $ls['id'] . "'>";
                $priceInputs .= " </td>";
            }
            $body .= "        <tr data-std='".$st['id']."'>";
            $body .= "            <td>" . $st['id'] . "</td>";
            $body .= "            <td>" . $st['firstname'] . "</td>";
            $body .= $priceInputs;
            $body .= "        </tr>";
             }

        $body .= "    </tbody>";
        $body.="</form>";
        return $body;
        }


}
