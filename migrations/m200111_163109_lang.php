<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m200111_163109_lang
 */
class m200111_163109_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lang',[
            'id'          => Schema::TYPE_PK,

            'url'       => Schema::TYPE_STRING ,
            'local'     => Schema::TYPE_STRING,
            'name'      => Schema::TYPE_STRING,
            'default'   => Schema::TYPE_INTEGER,

            'user_id'     => Schema::TYPE_INTEGER,
            'created_at'  => Schema::TYPE_INTEGER,
            'updated_at'  => Schema::TYPE_INTEGER,
        ]);

        $this->insert('lang',[
            'url'=>'uz',
            'local'=>'uz_UZ',
            'name'=>'uzbek',
            'default'=>1,
            'user_id'=>0,
            'created_at'=>time(),
            'updated_at'=>time()
        ]);
        $this->insert('lang',[
            'url'=>'ru',
            'local'=>'ru_RU',
            'name'=>'russian',
            'default'=>0,
            'user_id'=>0,
            'created_at'=>time(),
            'updated_at'=>time()
        ]);
        $this->insert('lang',[
            'url'=>'en',
            'local'=>'en_EN',
            'name'=>'english',
            'default'=>0,
            'user_id'=>0,
            'created_at'=>time(),
            'updated_at'=>time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200111_163109_lang cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200111_163109_lang cannot be reverted.\n";

        return false;
    }
    */
}
