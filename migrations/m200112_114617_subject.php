<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m200112_114617_subject
 */
class m200112_114617_subject extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('subject',[
            'id'          => Schema::TYPE_PK,

            'title'       => Schema::TYPE_STRING." NOT NULL",
            'category_id' => Schema::TYPE_INTEGER,

            'user_id'     => Schema::TYPE_INTEGER,
            'created_at'  => Schema::TYPE_INTEGER,
            'updated_at'  => Schema::TYPE_INTEGER,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200112_114617_subject cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200112_114617_subject cannot be reverted.\n";

        return false;
    }
    */
}
