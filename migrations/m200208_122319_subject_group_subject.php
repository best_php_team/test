<?php

use yii\db\Migration;

/**
 * Class m200208_122319_subject_group_subject
 */
class m200208_122319_subject_group_subject extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-subject-group_subject',
            'subject',
            'id',
            'group_subject',
            's_id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200208_122319_subject_group_subject cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200208_122319_subject_group_subject cannot be reverted.\n";

        return false;
    }
    */
}
