<?php

use yii\db\Migration;

/**
 * Class m200126_193321_rbac
 */
class m200126_193321_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('auth_assignment','created_at',\yii\db\Schema::TYPE_BIGINT);
        $this->execute(
            "INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
                ('ADMIN', 1, 'Administrator sevice ', NULL, NULL, 1578766066, 1578766066),
                ('Auth-index', 2, 'For view authorities index ppage', NULL, NULL, 1580048380, 1580048380),
                ('Category-index', 2, 'Category-index', NULL, NULL, 1580066386, 1580066386),
                ('DASHBOARD', 2, 'Dashboard view', NULL, NULL, 1580066337, 1580066337),
                ('DEVELOPER', 2, 'DEVELOPER', NULL, NULL, 1580066458, 1580066458),
                ('Journal-index', 2, 'Journal-index', NULL, NULL, 1580066348, 1580066348),
                ('MODERATOR', 2, 'Moderators service', NULL, NULL, 1580062994, 1580062994),
                ('ROOT', 1, 'ROOT', NULL, NULL, 1580066778, 1580066778),
                ('STUDENT', 1, 'O\'quvchi', NULL, NULL, 1580066765, 1580066765),
                ('Subject-index', 2, 'Subject-index', NULL, NULL, 1580066395, 1580066395),
                ('TEACHER', 1, 'O\'qituvchi', NULL, NULL, 1580066746, 1580066746),
                ('Test-all', 2, 'Test-all', NULL, NULL, 1580066358, 1580066358),
                ('Test-create', 2, 'Test-create', NULL, NULL, 1580066377, 1580066377),
                ('Test-pass', 2, 'Test-pass', NULL, NULL, 1580066367, 1580066367),
                ('User-create', 2, 'Create user account', NULL, NULL, 1578775680, 1578775680),
                ('User-delete', 2, 'Delete user', NULL, NULL, 1578765972, 1578765972),
                ('User-index', 2, 'User index', NULL, NULL, 1580066318, 1580066318),
                ('UserGroup-index', 2, 'UserGroup-index', NULL, NULL, 1580066404, 1580066404);
                COMMIT;"
        );
        $this->execute("
        INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
            ('ADMIN', 'Auth-index'),
            ('ADMIN', 'Category-index'),
            ('ADMIN', 'DEVELOPER'),
            ('ADMIN', 'Journal-index'),
            ('ADMIN', 'Test-all'),
            ('ADMIN', 'Test-create'),
            ('ADMIN', 'Test-pass'),
            ('ADMIN', 'User-create'),
            ('ADMIN', 'User-index'),
            ('ADMIN', 'UserGroup-index');
            COMMIT;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200126_193321_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200126_193321_rbac cannot be reverted.\n";

        return false;
    }
    */
}
