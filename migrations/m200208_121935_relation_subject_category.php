<?php

use yii\db\Migration;

/**
 * Class m200208_121935_relation_subject_category
 */
class m200208_121935_relation_subject_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-subject-category_id',
            'subject',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200208_121935_relation_subject_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200208_121935_relation_subject_category cannot be reverted.\n";

        return false;
    }
    */
}
