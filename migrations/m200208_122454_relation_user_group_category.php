<?php

use yii\db\Migration;

/**
 * Class m200208_122454_relation_user_group_category
 */
class m200208_122454_relation_user_group_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-user_group-category_id',
            'user_group',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200208_122454_relation_user_group_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200208_122454_relation_user_group_category cannot be reverted.\n";

        return false;
    }
    */
}
