<?php

use yii\db\Migration;

/**
 * Class m200208_124141_assign_role_to_admin
 */
class m200208_124141_assign_role_to_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('auth_assignment',[
            'item_name'=> 'ADMIN',
            'user_id'=>1,
            'created_at'=>time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200208_124141_assign_role_to_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200208_124141_assign_role_to_admin cannot be reverted.\n";

        return false;
    }
    */
}
