<?php

use yii\db\Migration;

/**
 * Class m200119_114118_questions
 */
class m200119_114118_questions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('questions', [
            'id'=>\yii\db\Schema::TYPE_PK,
            'question' => \yii\db\Schema::TYPE_STRING." NOT NULL",
            'lang' => \yii\db\Schema::TYPE_STRING." NOT NULL",
            'created_at' => \yii\db\Schema::TYPE_INTEGER,
            'updated_at' => \yii\db\Schema::TYPE_INTEGER,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200119_114118_questions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200119_114118_questions cannot be reverted.\n";

        return false;
    }
    */
}
