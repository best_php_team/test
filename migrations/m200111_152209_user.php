<?php

use app\models\User;
use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m200111_152209_user
 */
class m200111_152209_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user',[
            'id'                      => Schema::TYPE_PK,
            'login'                   => Schema::TYPE_STRING,
            'email'                   => Schema::TYPE_STRING   ,
            'user_group'              => Schema::TYPE_INTEGER,
            'role'                    => Schema::TYPE_INTEGER,
            'password_hash'           => Schema::TYPE_STRING,
            'password_reset_token'    => Schema::TYPE_STRING,
            'auth_key'                => Schema::TYPE_STRING,
            'status'                  => Schema::TYPE_INTEGER,
            'lastname'                => Schema::TYPE_STRING    ,
            'firstname'               => Schema::TYPE_STRING    ,
            'middlename'              => Schema::TYPE_STRING    ,
            'created_at'              => Schema::TYPE_INTEGER,
            'updated_at'              => Schema::TYPE_INTEGER,
        ]);
        $this->insert('user', [
            'login'                   => 'admin',
            'email'                   => 'admin@gmail.com',
            'password_hash'           => '$2y$13$.yeZHeCWuY5KI46VTfv0XOoWkU495TuxI5gTc7C1duUpmooUfTq62', // 1234
            'password_reset_token'    => 'syj5cBWagfQg1Li81xvldkVBBpi0GWBx_1574616522',
            'auth_key'                => 'fAHHWUbx1_Azp_SjvEbBxY-dxLgCtlT2',
            'status'                  => User::USER_ACTIVE,
            'lastname'                => 'Administrator',
            'firstname'               => 'Administrator',
            'middlename'              => 'Administrator',
            'created_at'              => time(),
            'updated_at'              => time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200111_152209_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200111_152209_user cannot be reverted.\n";

        return false;
    }
    */
}
