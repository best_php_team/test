<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m200111_150839_user_group
 */
class m200111_150839_user_group extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        $this->createTable('user_group',[
            'id'          => Schema::TYPE_PK,

            'title'       => Schema::TYPE_STRING." NOT NULL",
            'category_id' => Schema::TYPE_INTEGER,
            'user_id'     => Schema::TYPE_INTEGER,
            'created_at'  => Schema::TYPE_INTEGER,
            'updated_at'  => Schema::TYPE_INTEGER,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200111_150839_user_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200111_150839_user_group cannot be reverted.\n";

        return false;
    }
    */
}
