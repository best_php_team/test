<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m200111_143054_category
 */
class m200111_143054_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category',[
            'id'          => Schema::TYPE_PK,

            'title'       => Schema::TYPE_STRING." NOT NULL",

            'user_id'     => Schema::TYPE_INTEGER,
            'created_at'  => Schema::TYPE_INTEGER,
            'updated_at'  => Schema::TYPE_INTEGER,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200111_143054_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200111_143054_category cannot be reverted.\n";

        return false;
    }
    */
}
