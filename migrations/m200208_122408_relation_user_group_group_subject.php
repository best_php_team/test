<?php

use yii\db\Migration;

/**
 * Class m200208_122408_relation_user_group_group_subject
 */
class m200208_122408_relation_user_group_group_subject extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-user_group-group_subject',
            'user_group',
            'id',
            'group_subject',
            'g_id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200208_122408_relation_user_group_group_subject cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200208_122408_relation_user_group_group_subject cannot be reverted.\n";

        return false;
    }
    */
}
