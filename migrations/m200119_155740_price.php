<?php

use yii\db\Migration;

/**
 * Class m200119_155740_price
 */
class m200119_155740_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('price',[
            'id'=>\yii\db\Schema::TYPE_PK,
            'user_id'=>\yii\db\Schema::TYPE_INTEGER,
            'price'=>\yii\db\Schema::TYPE_STRING,
            'date'=>\yii\db\Schema::TYPE_INTEGER,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200119_155740_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200119_155740_price cannot be reverted.\n";

        return false;
    }
    */
}
