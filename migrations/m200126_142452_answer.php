<?php

use yii\db\Migration;

/**
 * Class m200126_142452_answer
 */
class m200126_142452_answer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('answers', [
            'id' => \yii\db\Schema::TYPE_PK,
            'label' => \yii\db\Schema::TYPE_INTEGER." NOT NULL",
            'answer' => \yii\db\Schema::TYPE_STRING." NOT NULL",
            'isright' => \yii\db\Schema::TYPE_INTEGER." NOT NULL",
            'question_id' => \yii\db\Schema::TYPE_INTEGER." NOT NULL",
            'created_at' => \yii\db\Schema::TYPE_INTEGER,
            'updated_at' => \yii\db\Schema::TYPE_INTEGER,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200126_142452_answer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200126_142452_answer cannot be reverted.\n";

        return false;
    }
    */
}
