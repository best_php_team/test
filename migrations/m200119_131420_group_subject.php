<?php

use yii\db\Migration;

/**
 * Class m200119_131420_group_subject
 */
class m200119_131420_group_subject extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('group_subject', [
            'g_id' => \yii\db\Schema::TYPE_INTEGER,
            's_id' => \yii\db\Schema::TYPE_INTEGER,
        ]);
        $this->createIndex('group_id', 'group_subject', 'g_id');
        $this->createIndex('subject_id', 'group_subject', 's_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200119_131420_group_subject cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200119_131420_group_subject cannot be reverted.\n";

        return false;
    }
    */
}
