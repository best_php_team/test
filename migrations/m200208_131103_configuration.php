<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m200208_131103_configuration
 */
class m200208_131103_configuration extends Migration
{
    private $tableName = "configurations";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{$this->tableName}",[
            'id'          => Schema::TYPE_PK,
            'title'       => Schema::TYPE_STRING." NOT NULL",
            'value' => Schema::TYPE_STRING,
            'created_at'  => Schema::TYPE_BIGINT,
            'updated_at'  => Schema::TYPE_BIGINT,
        ]);
        // test topshirishda bitt blokning maksimum sonini begilash uchun
        $this->insert("{$this->tableName}",[
            'title'=>'Testlar maksimum soni',
            'value'=>'10',
            'created_at'=>time(),
            'updated_at' =>time()
        ]);
        //
        $this->insert("{$this->tableName}",[
            'title'=>'Test topshirish vaqti (minutda)',
            'value'=>'30',
            'created_at'=>time(),
            'updated_at' =>time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200208_131103_configuration cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200208_131103_configuration cannot be reverted.\n";

        return false;
    }
    */
}
