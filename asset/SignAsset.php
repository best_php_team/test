<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\asset;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SignAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/web/css/bower_components/bootstrap/dist/css/bootstrap.min.css',
        '/web/css/bower_components/font-awesome/css/font-awesome.min.css',
        '/web/css/bower_components/Ionicons/css/ionicons.min.css',
        '/web/css/dist/css/AdminLTE.min.css',
        '/web/css/plugins/iCheck/square/blue.css',
        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
    ];
    public $js = [
//        "/web/css/bower_components/jquery/dist/jquery.min.js",
//        "/web/css/bower_components/bootstrap/dist/js/bootstrap.min.js",
        '/web/css/plugins/iCheck/icheck.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
