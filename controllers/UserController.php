<?php

namespace app\controllers;

use app\models\User;
use app\models\UserSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public  $model = 'User';    
    /**
     * {@inheritdoc}
     */
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['index', 'create', 'update', 'delete'],
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//        ];
//    }

    /**
     * @param $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
//    public function beforeAction($action)
//    {
//       if (\Yii::$app->user->can("{$this->model}-{$action->id}")) {
//        return parent::beforeAction($action);
//       } else {
//           throw new ForbiddenHttpException("Access denied");
//       }
//
//    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]); 
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new User();


        if ($post = Yii::$app->request->post('User')) {
            $model->login = $post['login'];
            $model->middlename = $post['middlename'];
            $model->firstname = $post['firstname'];
            $model->middlename = $post['middlename'];
            $model->generateAuthKey();
            $model->status = User::USER_ACTIVE;
            $model->user_group = $post['user_group'];
            $model->setPassword($post['password_hash']);
            $model->created_at = time();
            $model->updated_at = time();
            if ($model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success',  Yii::t('app','You  successfully registered new account!'));
                return $this->redirect(['index']);
            } else
            {
                Yii::$app->session->setFlash('error',  Yii::t('app','Somthing went wrong!'));
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }



    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($post = Yii::$app->request->post('User')) {
            $model->login = $post['login'];
            $model->generateAuthKey();
            $model->status = User::USER_ACTIVE;
            if(isset($post['password_hash']) && strlen($post['password_hash']) > 0)
                $model->setPassword($post['password_hash']);
            $model->user_group = $post['user_group'];
            $model->updated_at = time();
            if ($model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app','User account seccess updated!'));
                return $this->redirect(['index']);
            } else{
                Yii::$app->session->setFlash('error', Yii::t('app','Somthing went wrong!'));
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
                 
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success',  Yii::t('app','User account seccess deleted!'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }
        Yii::$app->session->setFlash('error', Yii::t('app','Something went wrong'));
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
