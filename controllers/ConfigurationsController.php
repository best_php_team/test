<?php

namespace app\controllers;

use Yii;
use app\models\Configurations;
use app\models\ConfigurationsSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ConfigurationsController implements the CRUD actions for Configurations model.
 */
class ConfigurationsController extends CRUDController
{
    public $model = "Configurations";
    public $image = false;

}
