<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 13.01.2020
 * Time: 0:56
 */

namespace app\controllers;

use Yii;
use app\models\Answers;
use app\models\Questions;
use app\models\QuestionsSearch;
use yii\widgets\ActiveForm;

class TestController extends Controller
{
    public function actionJournal()
    {
        return $this->render('index');
    }

    public function actionPass()
    {

        return $this->render('pass');
    }

    public function actionCreate()
    {
        $question = new Questions();
        $answer = new Answers();
        if($post = \Yii::$app->request->post()){

            echo "<pre>";
            var_dump(\Yii::$app->request->post());
            die;
            $question->question = $post['Questions']['question'];
            if($question->save()){

                for ($i=0;$i<=count($post['Answers']);$i++){
                    $ans = new Answers();
                    $ans->question_id = $question->id;
                    $ans->label = $post['Answers']['label'][$i];
                    $ans->isright = $post['Answers']['isright'][$i];
                    $ans->answer = $post['Answers']['answer'][$i];
                    if(!$ans->save()){
                        var_dump($ans->getErrors());
                    };
                }
                return $this->redirect('test/index');
            }else{
                var_dump($question->getErrors());
            }
        }
        return $this->render('create',[
            'answer'=>$answer,
            'question'=>$question
        ]);
    }

    public function actionAll(){
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id){
        $question = Questions::findOne(['id'=>$id]);
        $answer = Answers::findAll(['question_id'=>$id]);

        return $this->render('update',[
            'answer'=>$answer,
            'question'=>$question
        ]);
    }

    public function actionView($id){
        $question = Questions::findOne(['id'=>$id]);
        $answer = Answers::findAll(['question_id'=>$id]);

        return $this->render('view',[
            'answer'=>$answer,
            'question'=>$question
        ]);
    }

    public function actionAnswerRow(){
        return $this->renderPartial('answer-row', ['num'=>$_POST['num'], 'form'=>ActiveForm::begin(), 'answer'=>new Answers()]);
    }
}