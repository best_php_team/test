<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 16.03.2019
 * Time: 22:00
 */

namespace app\controllers;


use app\models\LoginForm;
use app\models\SignUp;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;


/**
 * Class SignController
 * @package app\controllers
 */
class SignController extends Controller
{
    /**
     * @var string
     */
    public $layout = '/sign';

    /**
     * @return string|\yii\web\Response
     */
    public function actionIn()
    {
        if (!Yii::$app->user->isGuest)
            return $this->redirect(Url::to("/site/index"));

        $model = new LoginForm();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post('LoginForm');
            $model->attributes = $post;

            if ($model->validate() && $model->login()) {
                Yii::$app->session->setFlash('success',Yii::t('app', 'Welcome {user}',['user'=>Yii::$app->user->identity->login]));
                return $this->redirect(Url::to("/site/index"));
            } else {
                return $this->render("/sign/in",['model'=>$model]);
            }
        }
        return $this->render('in', ['model' => $model]);

    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionUp()
    {
        $model = new SignUp();

        if ($post = Yii::$app->request->post()) {
            if ($model->load($post) && $model->validate() && $model->signup()){
                Yii::$app->session->setFlash('success', Yii::t('app', 'You are success logined!'));
                return $this->redirect("/sign/in");
            }
            else
            return $this->render("/sign/up",['model'=>$model]);
        }

        return $this->render('up', [
            'model' => $model
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionOut()
    {
        Yii::$app->user->logout();
        return $this->redirect(Url::to('/sign/in'));
    }

}