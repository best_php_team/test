<?php
/**
 * Created by PhpStorm.
 * User: Sanjar
 * Date: 19.01.2020
 * Time: 19:32
 */

namespace app\controllers;


use app\models\Price;
use app\models\SubjectSearch;
use app\models\User;
use app\models\UserGroup;
use app\models\UserGroupSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class JournalController extends Controller
{
    public function actionIndex(){
        $group = new UserGroupSearch();
        return $this->render('index', ['dataProvider'=>$group->search(['UserGroupSearch'=>\Yii::$app->request->queryParams]), 'searchModel'=>$group]);
    }

    public function actionSubjects($id){
        $subject = new SubjectSearch();
        return $this->render('subjects', ['id'=>$id, 'dataProvider'=>$subject->searchJournal(['SubjectSearch'=>\Yii::$app->request->queryParams], $id), 'searchModel'=>$subject]);
    }

    public function actionList($group, $subject){

        $students = [];
        $users = [];
        foreach (User::find()->where(['user_group'=>$group])->all() as $user){
            $students[] = ['id'=>$user->id, 'fio'=>$user->fio];
            $users[] = $user->id;
        }

        $lesson_dates = [];

        $p_ = ArrayHelper::map(Price::find()->where(['user_id'=>$users, 'subject_id'=>$subject])->all(), 'date', 'date');
        $tempdate = 0;
        foreach ($p_ as $p){
            $lesson_dates[] = $p;
            $tempdate = $p;
        }
        if($tempdate == strtotime(date('d.m.Y'))) $i = 1;
        else $i = 0;

        $tcount = 10-count($p_);
        for(; $i < $tcount; $i++){
            $tt = strtotime(date('d.m.Y'))+24*3600*$i;
            if(date('w', $tt)!=0)
                $lesson_dates[] = $tt;
            else $tcount++;
        }

        if(count($p_) == 10 && $tempdate!= strtotime(date('d.m.Y')))
            $lesson_dates[] = strtotime(date('d.m.Y'));

        $prices = [];

        foreach (Price::find()->where(['user_id'=>$users, 'subject_id'=>$subject])->all() as $p){
            $prices[$p->user_id][$p->date] = $p->price;
        }

        return $this->render('list', ['students'=>$students, 'lesson_dates'=>$lesson_dates, 'prices'=>$prices, 'group'=>$group, 'subject'=>$subject]);
    }

    public function actionPrice($group, $subject){
        if(is_array(\Yii::$app->request->post('price')))
            foreach (\Yii::$app->request->post('price') as $u_id=>$p){
                foreach ($p as $date=>$price){
                    $temp1 = Price::find()->where(['user_id'=>$u_id, 'date'=>$date, 'price'=>$price])->one();
                    if(!$temp1){
                        $temp2 = Price::find()->where(['user_id'=>$u_id, 'date'=>$date])->one();
                        if(!$temp2){
                            $temp4 = new Price();
                            $temp4->date = $date;
                            $temp4->user_id = $u_id;
                            $temp4->price = $price;
                            $temp4->save();
                        }else{
                            $temp2->price = $price;
                            $temp2->save();
                        }
                    }

                }
            }
        return $this->redirect(['list', 'group'=>$group, 'subject'=>$subject]);
    }

}